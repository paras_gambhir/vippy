'use strict';
/**
 * Created by prince on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../Config');

var non_auth_routes = [

   {
        method: 'POST',
        path: '/api/admin/login',
        config: {
            description: 'Login for Admin',
            tags: ['api', 'admin'],
            handler: function (request, reply) {
                var queryData = {
                    email: request.payload.email,
                    password: request.payload.password
                };
                Controller.AdminController.adminLogin(queryData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err))
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                })
            },
            validate: {
                failAction: UniversalFunctions.failActionFunction,
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().required()
                }
            },
            plugins: {
                'hapi-swagger': {
                    payloadType:'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/addTicket',
        config: {
            description: 'Add Ticket',
            tags: ['api', 'admin','add ticket'],
            handler: function (request, reply) {
                var payloadData =  request.payload;
                Controller.AdminController.addTicket(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data));
                    }
                })
            },
            payload:{
                maxBytes:10485760,
                parse: true,
                output: 'file',
                allow: 'multipart/form-data'
            },
            validate: {
                failAction: UniversalFunctions.failActionFunction,
                payload: {
                    accessToken: Joi.string().required().trim(),
                    seedGeekEventId:Joi.string().required().trim(),
                    images: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('image file'),
                    seedGeekVenueId:Joi.string().required().trim(),
                    seedGeekArtistId:Joi.string().optional().trim(),
                    ticketType:Joi.string().required().valid([Config.APP_CONSTANTS.DATABASE.TICKETTYPE.GA,
                        Config.APP_CONSTANTS.DATABASE.TICKETTYPE.VIP]),
                    venueName:Joi.string().required().trim(),
                    artistName:Joi.string().optional().trim(),
                    eventName:Joi.string().required().trim(),
                    eventDate:Joi.string().required().trim(),
                    ticketPrice:Joi.number().required(),
                    ticketQuantity:Joi.number().required(),
                    timings:Joi.string().required(),
                    expiryDate:Joi.string().required().trim(),
                    latitude:Joi.number().required(),
                    longitude:Joi.number().required()
                }
            },
            plugins: {
                'hapi-swagger': {
                    payloadType:'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/imageUploadArtistVenue',
        config: {
            description: 'imageUploadArtistVenue ',
            tags: ['api', 'admin','image Upload Artist Venue'],
            handler: function (request, reply) {
                var payloadData =  request.payload;
                Controller.AdminController.imageUploadArtistVenue(payloadData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data));
                    }
                })
            },
            payload:{
                maxBytes:10485760,
                parse: true,
                output: 'file',
                allow: 'multipart/form-data'
            },
            validate: {
                failAction: UniversalFunctions.failActionFunction,
                payload: {
                    accessToken: Joi.string().required().trim(),
                    images: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('image file'),
                    type:Joi.string().required().description('0 for artist 1 for venue')
                }
            },
            plugins: {
                'hapi-swagger': {
                    payloadType:'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/searchEvents',
        config: {
            description: 'Search for events',
            tags: ['api', 'admin','search for event'],
            handler: function (request, reply) {

                var queryData = request.query;

                Controller.AdminController.searchForEvents(queryData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err))
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                })
            },
            validate: {
                failAction: UniversalFunctions.failActionFunction,
                query: {
                    accessToken: Joi.string().required(),
                    searchString: Joi.string().required()
                }
            },
            plugins: {
                'hapi-swagger': {
                    payloadType:'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/searchForArtistAndVenueEventWise',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.searchForArtistEventWise(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'Search For Artist Event Wise',
        tags: ['api', 'admin', 'search for artist event wise'],

        validate: {
            query: {
                accessToken: Joi.string().required().trim(),
                eventId: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'GET',
        path: '/api/admin/listTickets',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.AdminController.listTickets(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'List Tickets',
        tags: ['api', 'admin', 'list tickets'],

        validate: {
            query: {
                accessToken: Joi.string().required(),
                uploadedBy: Joi.string().required().valid(
                    ['ADMIN','USER'])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    }

];

var adminLogin = [

];


var authRoutes = [].concat(adminLogin);

module.exports = authRoutes.concat(non_auth_routes);