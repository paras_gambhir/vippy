var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../Config');
module.exports = [

    {
        method: 'POST',
        path: '/api/user/signup',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.userSignUp(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Sign Up',
        tags: ['api', 'user', 'signup'],
        validate: {
            payload: {
                firstName: Joi.string().required().trim(),
                lastName: Joi.string().required().trim(),
                userName: Joi.string().required().trim(),
                email: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim(),
                facebookId: Joi.string().optional(),
                password: Joi.string().required().trim(),
                appVersion: Joi.string().required(),
                latitude: Joi.number().required(),
                longitude: Joi.number().required(),
                deviceToken: Joi.string().required(),
                imageUrl: Joi.string().optional(),
                deviceType: Joi.string().required().valid(
                    [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/fbLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.fbLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Fb login',
        tags: ['api', 'user', 'fb login'],
        validate: {
            payload: {
                facebookId: Joi.string().required(),
                appVersion: Joi.string().required(),
                latitude: Joi.number().required(),
                longitude: Joi.number().required(),
                deviceToken: Joi.string().required(),
                deviceType: Joi.string().required().valid(
                    [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/emailLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.emailLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Email login',
        tags: ['api', 'user', 'email login'],
        validate: {
            payload: {
                email: Joi.string().required(),
                password: Joi.string().required(),
                appVersion: Joi.string().required(),
                latitude: Joi.number().required(),
                longitude: Joi.number().required(),
                deviceToken: Joi.string().required(),
                deviceType: Joi.string().required().valid(
                    [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/accessTokenLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.accessTokenLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Access Token login',
        tags: ['api', 'user', 'access token login'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                appVersion: Joi.string().required(),
                latitude: Joi.number().required(),
                longitude: Joi.number().required(),
                deviceToken: Joi.string().required(),
                deviceType: Joi.string().required().valid(
                    [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/checkForEmailAndPhone',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.checkForEmailAndPhone(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'Check for email and phone number',
        tags: ['api', 'user', 'check email'],
        validate: {
            payload: {
                email: Joi.string().required().trim(),
                phoneNumber: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/forgotPassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.forgotPassword(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Forgot Password',
        tags: ['api', 'user', 'forgot password'],
        validate: {
            payload: {
                email: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'GET',
        path: '/api/user/searchSuggestions',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.searchSuggestions(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Search Suggestions',
        tags: ['api', 'user', 'search suggestions'],
        validate: {
            query: {
                accessToken: Joi.string().optional(),
                latitude: Joi.string().required(),
                longitude: Joi.string().required(),
                searchString: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'GET',
        path: '/api/user/loadTicketResultsForArtists',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.loadTicketsOfArtists(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Load Ticket Results for Artists',
        tags: ['api', 'user', 'artist tickets load'],
        validate: {
            query: {
                accessToken: Joi.string().optional(),
                latitude: Joi.string().required(),
                longitude: Joi.string().required(),
                artistId: Joi.number().required(),
                eventDate: Joi.string().optional(),
                ticketType: Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.TICKETTYPE.GA,
                    Config.APP_CONSTANTS.DATABASE.TICKETTYPE.VIP]),
                searchString: Joi.string().optional()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'GET',
        path: '/api/user/loadTicketResultsForVenues',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.loadTicketsOfVenues(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Load Ticket Results for Venues',
        tags: ['api', 'user', 'venues tickets load'],
        validate: {
            query: {
                accessToken: Joi.string().optional(),
                venueId: Joi.number().required(),
                eventDate: Joi.string().optional(),
                ticketType: Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.TICKETTYPE.GA,
                    Config.APP_CONSTANTS.DATABASE.TICKETTYPE.VIP]),
                searchString: Joi.string().optional()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'GET',
        path: '/api/user/getProfile',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.getProfile(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Load Ticket Results for Venues',
        tags: ['api', 'user', 'venues tickets load'],
        validate: {
            query: {
                accessToken: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/updateProfile',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.updateProfile(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        payload: {
            maxBytes: 10485760,
            parse: true,
            output: 'file'
        },
        description: 'User Update Profile',
        tags: ['api', 'user', 'update profile'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                firstName: Joi.string().required().trim(),
                lastName: Joi.string().required().trim(),
                profilePic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .description('image file')

            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'PUT',
        path: '/api/user/changePassword',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.changePassword(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Reset/change Password',
        tags: ['api', 'user', 'reset/change password'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                oldPassword: Joi.string().required().trim(),
                newPassword: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'GET',
        path: '/api/user/getCards',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.getCards(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Get Credit Card Info',
        tags: ['api', 'user', 'credit card'],
        validate: {
            query: {
                accessToken: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/addCard',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.addCard(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Add Credit Card',
        tags: ['api', 'user', 'credit card', 'add'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                stripeToken: Joi.string().required().trim(),
                cardType: Joi.string().required().trim(),
                last4Digit: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/deleteCard',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.deleteCard(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Delete Credit Card ',
        tags: ['api', 'user', 'credit card', 'delete'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                cardId: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/changeDefaultCard',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.changeDefaultCard(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Change Default Credit Card',
        tags: ['api', 'user', 'credit card', 'default'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                cardId: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'GET',
        path: '/api/user/ticketDetails',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.getTicketDetails(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'Ticket Details of particular event and venue',
        tags: ['api', 'user', 'ticket details', 'event/venue'],
        validate: {
            query: {
                accessToken: Joi.string().required().trim(),
                venueId: Joi.number().required(),
                eventId: Joi.number().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/purchaseTicket',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.purchaseTicket(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Purchase Tickets',
        tags: ['api', 'user', 'purchase ticket'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                ticketData: Joi.string().required(),
                totalAmount: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/listUserTickets',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.listUserTickets(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'List User Tickets',
        tags: ['api', 'user', 'list tickets'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/logout',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.userLogout(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Logout',
        tags: ['api', 'user', 'logout'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/sellYourTicket',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.sellYourTicket(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Sell Your Ticket from My Tickets Menu',
        tags: ['api', 'user', 'sell ticket'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                ticketId: Joi.string().required().trim(),
                id: Joi.string().required().trim(),
                ticketType:Joi.string().required().valid(
                    [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USERTICKETTYPE.PURCHASED,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USERTICKETTYPE.UPLOADED])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'GET',
        path: '/api/user/listUsers',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.listUsers(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'List Users on Popup',
        tags: ['api', 'user', 'list users'],
        validate: {
            query: {
                accessToken: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/transferTicket',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.transferTicket(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'Transfer Ticket',
        tags: ['api', 'user', 'transfer ticket'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                ticketId: Joi.string().required().trim(),
                id: Joi.string().required().trim(),
                userId: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/revertBackTicket',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.revertBackTicket(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'Revert Back Ticket',
        tags: ['api', 'user', 'revert back ticket'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                ticketId: Joi.string().required().trim(),
                id: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/uploadTicket',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.uploadTicket(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        },
       config: {
           payload: {
               maxBytes: 10485760,
               parse: true,
               output: 'file',
               allow: 'multipart/form-data'
           },
        description: 'Upload Ticket',
        tags: ['api', 'user', 'upload ticket'],

        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                eventId: Joi.string().required().trim(),
                eventName: Joi.string().required().trim(),
                artistId: Joi.string().optional(),
                artistName: Joi.string().optional(),
                venueId: Joi.string().required(),
                venueName: Joi.string().required(),
                eventDate: Joi.string().required().trim(),
                ticketImage: Joi.any()
                    .meta({swaggerType: 'file'})
                    .description('image file'),
                isReadByOCR: Joi.boolean().required(),
                latitude: Joi.string().required(),
                longitude: Joi.string().required(),
                ticketType: Joi.string().required().valid([Config.APP_CONSTANTS.DATABASE.TICKETTYPE.GA,
                    Config.APP_CONSTANTS.DATABASE.TICKETTYPE.VIP]),
                ticketPrice: Joi.string().required(),
                timings:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'GET',
        path: '/api/user/searchForEventsWhileUpload',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.searchForEvents(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'Search For Event While Uploading Ticket',
        tags: ['api', 'user', 'search for event while uploading'],

        validate: {
            query: {
                accessToken: Joi.string().required().trim(),
                eventName: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'GET',
        path: '/api/user/searchForArtistAndVenueEventWise',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.searchForArtistEventWise(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'Search For Artist Event Wise',
        tags: ['api', 'user', 'search for artist event wise'],

        validate: {
            query: {
                accessToken: Joi.string().required().trim(),
                eventId: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/user/sellTicketOnSellingScreen',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.UserController.sellTicketOnSellingScreen(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        },
        config: {
            payload: {
                maxBytes: 10485760,
                parse: true,
                output: 'file',
                allow: 'multipart/form-data'
            },
            description: 'Sell Ticket on selling screen',
            tags: ['api', 'user', 'sell ticket'],

            validate: {
                payload: {
                    accessToken: Joi.string().required().trim(),
                    eventId: Joi.string().required().trim(),
                    eventName: Joi.string().required().trim(),
                    artistId: Joi.string().optional(),
                    artistName: Joi.string().optional(),
                    venueId: Joi.string().required(),
                    venueName: Joi.string().required(),
                    eventDate: Joi.string().required().trim(),
                    ticketImage: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('image file'),
                    isReadByOCR: Joi.boolean().required(),
                    latitude: Joi.string().required(),
                    longitude: Joi.string().required(),
                    ticketType: Joi.string().required().valid([Config.APP_CONSTANTS.DATABASE.TICKETTYPE.GA,
                        Config.APP_CONSTANTS.DATABASE.TICKETTYPE.VIP]),
                    ticketPrice: Joi.string().required(),
                    timings:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/user/getSuggestedPrice',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.UserController.getSuggestedPrice(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'Suggested Price of Ticket while selling',
        tags: ['api', 'user', 'suggested price of ticket'],

        validate: {
            query: {
                accessToken: Joi.string().required().trim(),
                eventId: Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    }

];