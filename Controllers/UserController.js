var Service = require('../Services');
var async = require('async');


var TokenManager = require('../Lib/TokenManager');
var UploadManager = require('../Lib/UploadManager');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var NotificationManager = require('../Lib/NotificationManager');
var Config = require('../Config/seatGeekConfig');
var appConstants = require('../Config/appConstants');
var mongoose = require('mongoose');
var _ = require('lodash');

var CodeGenerator = require('../Lib/CodeGenerator');

var stripe = require("stripe")(
    appConstants.SERVER.STRIPE_KEY
);

var request = require('request');


var userSignUp = function (payloadData, callback) {
    var userFound = false;
    var userData;
    var accessToken;
    async.auto({
        checkForEmailAndFbId: function (cb) {

            if (payloadData.facebookId) {
                var criteria = {
                    facebookId: payloadData.facebookId
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.UserService.getUser(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result.length) {
                            userFound = true;
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_EXISTS);

                        }
                        else {
                            var criteria = {
                                email: payloadData.email
                            };
                            var projection = {};
                            var option = {
                                lean: true
                            };
                            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                                if (err) {
                                    cb(err)
                                } else {

                                    if (result.length) {
                                        userFound = true;
                                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);
                                    }
                                    else {
                                        cb();
                                    }

                                }
                            })

                        }
                    }
                });
            }
            else {
                var criteria = {
                    email: payloadData.email
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.UserService.getUser(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {

                        if (result.length) {
                            userFound = true;
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);
                        }
                        else {
                            cb();
                        }

                    }
                })
            }

        },
        checkForUserName: ['checkForEmailAndFbId', function (cb) {
            var criteria = {
                userName: payloadData.userName
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {

                    if (result.length) {
                        userFound = true;
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.USERNAME_EXISTS);
                    }
                    else {
                        cb();
                    }

                }
            })

        }],
        checkForPhoneNumber: ['checkForUserName', function (cb) {
            var criteria = {
                phoneNumber: payloadData.phoneNumber
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {

                    if (result.length) {
                        userFound = true;
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_EXISTS);
                    }
                    else {
                        cb();
                    }

                }
            })
        }],
        insertUser: ['checkForPhoneNumber', function (cb) {
            var dataToBeInserted = {
                firstName: payloadData.firstName,
                lastName: payloadData.lastName,
                userName: payloadData.userName,
                countryCode: payloadData.countryCode,
                phoneNumber: payloadData.phoneNumber,
                facebookId: payloadData.facebookId ? payloadData.facebookId : "",
                appVersion: payloadData.appVersion,
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType,
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                password: UniversalFunctions.CryptData(payloadData.password),
                email: payloadData.email,
                profilePicURL: {
                    original: payloadData.facebookId ? "http://graph.facebook.com/" + payloadData.facebookId + "/picture?type=large" : 'https://s3-us-west-2.amazonaws.com/vippy/default-person.jpg',
                    thumbnail: payloadData.facebookId ? "http://graph.facebook.com/" + payloadData.facebookId + "/picture?type=large" : 'https://s3-us-west-2.amazonaws.com/vippy/default-person.jpg'
                }

            };
            Service.UserService.createUser(dataToBeInserted, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userData = result;
                    cb();
                }
            });
        }],
        setToken: ['insertUser', function (cb) {
            var tokenData = {
                id: userData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        userData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: userData});
        }
    })
};


var fbLogin = function (payloadData, callback) {
    var userData;
    var accessToken;
    async.auto({
        checkForFbId: function (cb) {
            var criteria = {
                facebookId: payloadData.facebookId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }
            });
        },
        updateUser: ['checkForFbId', function (cb) {
            var criteria = {
                id: userData._id
            };
            var setQuery = {
                appVersion: payloadData.appVersion,
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType
            };
            Service.UserService.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }],
        setToken: ['updateUser', function (cb) {
            var tokenData = {
                id: userData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        userData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: userData});
        }
    })

};


var emailLogin = function (payloadData, callback) {
    var userData;
    var accessToken;
    async.auto({
        checkForEmail: function (cb) {
            var criteria = {
                email: payloadData.email,
                password: UniversalFunctions.CryptData(payloadData.password)
            };

            console.log(UniversalFunctions.CryptData(payloadData.password))
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb();
                    }
                }
            });
        },
        checkForUserName: ['checkForEmail', function (cb) {
            if (userData) {
                cb();
            }
            else {
                var criteria = {
                    userName: payloadData.email,
                    password: UniversalFunctions.CryptData(payloadData.password)
                };

                var projection = {};
                var option = {
                    lean: true
                };
                Service.UserService.getUser(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result.length) {
                            userData = result[0];
                            cb();
                        }
                        else {
                            cb();
                        }
                    }
                });
            }

        }],
        checkForPhone: ['checkForUserName', function (cb) {
            if (userData) {
                cb();
            }
            else {
                var criteria = {
                    phoneNumber: payloadData.email,
                    password: UniversalFunctions.CryptData(payloadData.password)
                };

                var projection = {};
                var option = {
                    lean: true
                };
                Service.UserService.getUser(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result.length) {
                            userData = result[0];
                            cb();
                        }
                        else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS);
                        }
                    }
                });
            }
        }],
        updateUser: ['checkForPhone', function (cb) {
            var criteria = {
                id: userData._id
            };
            var setQuery = {
                appVersion: payloadData.appVersion,
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType
            };
            Service.UserService.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }],
        setToken: ['updateUser', function (cb) {
            var tokenData = {
                id: userData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        userData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: userData});
        }
    })
};


var forgotPassword = function (payloadData, callback) {
    var userData;
    var newPassword = "";
    async.auto({
        checkForEmail: function (cb) {
            var criteria = {
                email: payloadData.email
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_NOT_REGISTERED);
                    }
                }
            });
        },
        setNewPassword: ['checkForEmail', function (cb) {
            newPassword = UniversalFunctions.generatePassword();
            var encryptedPassword = UniversalFunctions.CryptData(newPassword);
            var criteria = {
                email: payloadData.email
            };
            var setQuery = {
                password: encryptedPassword
            };
            var option = {
                new: true
            };
            Service.UserService.updateUser(criteria, setQuery, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }],
        sendMail: ['setNewPassword', function (cb) {
            NotificationManager.sendEmailToUser('FORGOT_PASSWORD', newPassword, payloadData.email, function (callback) {
                cb();

            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {});
        }

    })

};


var accessTokenLogin = function (payloadData, callback) {
    var userData;
    var accessToken;
    async.auto({
        checkForAccessToken: function (cb) {


            var criteria = {
                accessToken: payloadData.accessToken
            };


            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        updateUser: ['checkForAccessToken', function (cb) {
            var criteria = {
                id: userData._id
            };
            var setQuery = {
                appVersion: payloadData.appVersion,
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType
            };
            Service.UserService.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }],
        setToken: ['updateUser', function (cb) {
            var tokenData = {
                id: userData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        userData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {userData: userData});
        }
    })
};


var checkForEmailAndPhone = function (payloadData, callback) {

    async.auto({
        checkForEmail: function (cb) {
            var criteria = {
                email: payloadData.email
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    if (result.length) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);
                    }
                    else {
                        cb();
                    }
                }
            });
        },
        checkForPhone: ['checkForEmail', function (cb) {
            var criteria = {
                phoneNumber: payloadData.phoneNumber
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_EXISTS);
                    }
                    else {
                        cb();
                    }
                }
            });


        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })

};


var searchSuggestions = function (payloadData, callback) {
    var searchResults = [];
    var userData;
    async.auto({
        checkForAccessToken: function (cb) {

            if (payloadData.accessToken) {
                var criteria = {
                    accessToken: payloadData.accessToken
                };

                var projection = {};
                var option = {
                    lean: true
                };
                Service.UserService.getUser(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result.length) {
                            userData = result[0];
                            cb();
                        }
                        else {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                        }
                    }
                });
            }
            else {
                cb();
            }
        },
        searchForVenues: ['checkForAccessToken', function (cb) {

            request(Config.seatGeek.baseUrl + "venues?client_id=" + Config.seatGeek.clientId + "&client_secret=" + Config.seatGeek.clientSecret + "&per_page=5&name=" + payloadData.searchString, function (err, data, response) {
                if (err) {
                    cb(err);
                }
                else {
                    response = JSON.parse(response);
                    response.venues.forEach(function (result) {
                        searchResults.push({
                            "id": result.id,
                            "name": result.name,
                            "type": "Venue"
                        });
                    });
                    cb();
                }
            })
        }],
        searchForArtists: ['checkForAccessToken', function (cb) {
            request(Config.seatGeek.baseUrl + "performers?client_id=" + Config.seatGeek.clientId + "&client_secret=" + Config.seatGeek.clientSecret + "&per_page=5&q=" + payloadData.searchString, function (err, data, response) {
                if (err) {
                    cb(err);
                }
                else {
                    console.log("================================", response);
                    response = JSON.parse(response);
                    console.log(response);
                    response.performers.forEach(function (result) {
                        searchResults.push({
                            "id": result.id,
                            "name": result.name,
                            "type": "Artist"
                        });

                    });
                    cb();
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {searchResults: searchResults})
        }
    })
};


var loadTicketsOfArtists = function (payloadData, callback) {


    var nearbyTickets = [];
    var allTickets = [];
    var artistImage = "";
    async.auto({
        checkForAccessToken: function (cb) {

            if (payloadData.accessToken) {
                var criteria = {
                    accessToken: payloadData.accessToken
                };

                var projection = {};
                var option = {
                    lean: true
                };
                Service.UserService.getUser(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result.length) {
                            console.log("user");
                            cb();
                        }
                        else {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                        }
                    }
                });
            }
            else {
                cb();
            }
        },
        getAllTickets: ['checkForAccessToken', function (cb) {

            if (!payloadData.ticketType && !payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (!payloadData.ticketType && !payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
                console.log(criteria);
            }

            else if (payloadData.ticketType && payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            eventDate: payloadData.eventDate,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            eventDate: payloadData.eventDate,
                            $text: {$search: "/" + payloadData.searchString + "/"},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            eventDate: payloadData.eventDate,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }


            else if (payloadData.ticketType && !payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && !payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }


            else if (!payloadData.ticketType && payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            eventDate: payloadData.eventDate,
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (!payloadData.ticketType && payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            eventDate: payloadData.eventDate,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            Service.TicketService.getTicketAggregate(criteria, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("result all", result);
                    allTickets = result;
                    if (allTickets.length) {
                        for (var i = 0; i < allTickets.length; i++) {
                            (function (i) {
                                allTickets[i].eventDate = dateChangeToFormat(allTickets[i].eventDate);
                                if (i == allTickets.length - 1) {
                                    cb();
                                }

                            }(i))
                        }
                    }
                    else {
                        cb();
                    }
                }
            });
        }],
        getNearbyTickets: ['checkForAccessToken', function (cb) {

            if (!payloadData.ticketType && !payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (!payloadData.ticketType && !payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            eventDate: payloadData.eventDate,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            eventDate: payloadData.eventDate,
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            eventDate: payloadData.eventDate,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }


            else if (payloadData.ticketType && !payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && !payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }


            else if (!payloadData.ticketType && payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            eventDate: payloadData.eventDate,
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (!payloadData.ticketType && payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekArtistId: payloadData.artistId,
                            quantityLeft: {$gt: 0},
                            eventDate: payloadData.eventDate,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"venueId": '$seedGeekVenueId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            venueName: {$first: '$venueName'},
                            seedGeekArtistId: {$first: '$seedGeekArtistId'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekVenueId: '$_id.venueId',
                            seedGeekArtistId: '$seedGeekArtistId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            Service.TicketService.getTicketAggregate(criteria, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("result near", result);
                    nearbyTickets = result;
                    if (nearbyTickets.length) {
                        for (var i = 0; i < nearbyTickets.length; i++) {
                            (function (i) {
                                nearbyTickets[i].eventDate = dateChangeToFormat(nearbyTickets[i].eventDate);
                                if (i == nearbyTickets.length - 1) {
                                    cb();
                                }

                            }(i))
                        }
                    }
                    else {
                        cb();
                    }
                }
            });
        }],

        getArtistImage: ['checkForAccessToken', function (cb) {
            var criteria = {};

            var projection = {};
            var option = {
                lean: true
            };
            Service.editableFieldService.getEditableFields(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        console.log("data aaja", result);
                        artistImage = result[0].artistImage;
                        cb();
                    }
                    cb();
                }
            });
        }]

    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {nearbyTickets: nearbyTickets, allTickets: allTickets, artistImage: artistImage})
        }

    })
};


var loadTicketsOfVenues = function (payloadData, callback) {
    console.log(payloadData);
    var upcomingTickets = [];
    var allTickets = [];
    var venueImage = "";
    var nowDate = new Date();
    async.auto({
        checkForAccessToken: function (cb) {

            if (payloadData.accessToken) {
                var criteria = {
                    accessToken: payloadData.accessToken
                };

                var projection = {};
                var option = {
                    lean: true
                };
                Service.UserService.getUser(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result.length) {
                            cb();
                        }
                        else {
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                        }
                    }
                });
            }
            else {
                cb();
            }
        },

        getUpcomingTickets: ['checkForAccessToken', function (cb) {

            if (payloadData.ticketType && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            eventDate: {$gt: nowDate},
                            ticketType: payloadData.ticketType,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    },
                    {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            eventDate: {$gt: nowDate},
                            ticketType: payloadData.ticketType,
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    },
                    {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (!payloadData.ticketType && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            eventDate: {$gt: nowDate},
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    },
                    {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (!payloadData.ticketType && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            eventDate: {$gt: nowDate},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    },
                    {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            eventDate: {$gt: nowDate},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }
                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    },
                    {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }


            Service.TicketService.getTicketAggregate(criteria, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("result upcoming", result);
                    upcomingTickets = result;
                    if (upcomingTickets.length) {
                        for (var i = 0; i < upcomingTickets.length; i++) {
                            (function (i) {
                                upcomingTickets[i].eventDate = dateChangeToFormat(upcomingTickets[i].eventDate);
                                if (i == upcomingTickets.length - 1) {
                                    cb();
                                }

                            }(i))
                        }
                    }
                    else {
                        cb();
                    }
                }
            });

        }],

        getAllTickets: ['checkForAccessToken', function (cb) {

            // if (payloadData.ticketType && payloadData.eventDate && payloadData.searchString) {
            //     var criteria = [
            //         {
            //             $match: {seedGeekVenueId: payloadData.venueId, quantityLeft: {$gt: 0}},
            //             ticketType: payloadData.ticketType,
            //             eventDate: payloadData.eventDate,
            //             $text: {$search: payloadData.searchString}
            //
            //         },
            //         {
            //             $group: {
            //                 originalId: {$first: '$_id'}, // Hold onto original ID.
            //                 _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
            //                 seedGeekVenueId: {$first: '$seedGeekVenueId'},
            //                 venueName: {$first: '$venueName'},
            //                 artistName: {$first: '$artistName'},
            //                 latitude: {$first: '$latitude'},
            //                 longitude: {$first: '$longitude'},
            //                 ticketName: {$first: '$ticketName'},
            //                 ticketPrice: {$first: '$ticketPrice'},
            //                 ticketType: {$first: '$ticketType'},
            //                 quantityLeft: {$first: '$quantityLeft'},
            //                 eventDate: {$first: '$eventDate'},
            //                 timings: {$first: '$timings'}
            //             }
            //
            //         }, {
            //             // this receives the output from the first aggregation.
            //             // So the (originally) non-unique 'id' field is now
            //             // present as the _id field. We want to rename it.
            //             $project: {
            //                 _id: '$originalId', // Restore original ID.
            //                 seedGeekEventId: '$_id.eventId',
            //                 seedGeekArtistId: '$_id.artistId', //
            //                 seedGeekVenueId: '$seedGeekVenueId',
            //                 venueName: '$venueName',
            //                 artistName: '$artistName',
            //                 latitude: '$latitude',
            //                 longitude: '$longitude',
            //                 ticketName: '$ticketName',
            //                 ticketPrice: '$ticketPrice',
            //                 ticketType: '$ticketType',
            //                 quantityLeft: '$quantityLeft',
            //                 eventDate: '$eventDate',
            //                 timings: '$timings'
            //             }
            //         }
            //
            //     ];
            // }
            //
            // else if (payloadData.ticketType && !payloadData.eventDate) {
            //     var criteria = [
            //         {
            //             $match: {
            //                 seedGeekVenueId: payloadData.venueId,
            //                 quantityLeft: {$gt: 0},
            //                 ticketType: payloadData.ticketType
            //             }
            //         },
            //         {
            //             $group: {
            //                 originalId: {$first: '$_id'}, // Hold onto original ID.
            //                 _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
            //                 seedGeekVenueId: {$first: '$seedGeekVenueId'},
            //                 venueName: {$first: '$venueName'},
            //                 artistName: {$first: '$artistName'},
            //                 latitude: {$first: '$latitude'},
            //                 longitude: {$first: '$longitude'},
            //                 ticketName: {$first: '$ticketName'},
            //                 ticketPrice: {$first: '$ticketPrice'},
            //                 ticketType: {$first: '$ticketType'},
            //                 quantityLeft: {$first: '$quantityLeft'},
            //                 eventDate: {$first: '$eventDate'},
            //                 timings: {$first: '$timings'}
            //             }
            //
            //         }, {
            //             // this receives the output from the first aggregation.
            //             // So the (originally) non-unique 'id' field is now
            //             // present as the _id field. We want to rename it.
            //             $project: {
            //                 _id: '$originalId', // Restore original ID.
            //                 seedGeekEventId: '$_id.eventId',
            //                 seedGeekArtistId: '$_id.artistId', //
            //                 seedGeekVenueId: '$seedGeekVenueId',
            //                 venueName: '$venueName',
            //                 artistName: '$artistName',
            //                 latitude: '$latitude',
            //                 longitude: '$longitude',
            //                 ticketName: '$ticketName',
            //                 ticketPrice: '$ticketPrice',
            //                 ticketType: '$ticketType',
            //                 quantityLeft: '$quantityLeft',
            //                 eventDate: '$eventDate',
            //                 timings: '$timings'
            //             }
            //         }
            //
            //     ];
            // }
            //
            // else if (!payloadData.ticketType && payloadData.eventDate) {
            //     var criteria = [
            //         {
            //             $match: {
            //                 seedGeekVenueId: payloadData.venueId,
            //                 quantityLeft: {$gt: 0},
            //                 eventDate: payloadData.eventDate
            //             }
            //         },
            //         {
            //             $group: {
            //                 originalId: {$first: '$_id'}, // Hold onto original ID.
            //                 _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
            //                 seedGeekVenueId: {$first: '$seedGeekVenueId'},
            //                 venueName: {$first: '$venueName'},
            //                 artistName: {$first: '$artistName'},
            //                 latitude: {$first: '$latitude'},
            //                 longitude: {$first: '$longitude'},
            //                 ticketName: {$first: '$ticketName'},
            //                 ticketPrice: {$first: '$ticketPrice'},
            //                 ticketType: {$first: '$ticketType'},
            //                 quantityLeft: {$first: '$quantityLeft'},
            //                 eventDate: {$first: '$eventDate'},
            //                 timings: {$first: '$timings'}
            //             }
            //
            //         }, {
            //             // this receives the output from the first aggregation.
            //             // So the (originally) non-unique 'id' field is now
            //             // present as the _id field. We want to rename it.
            //             $project: {
            //                 _id: '$originalId', // Restore original ID.
            //                 seedGeekEventId: '$_id.eventId',
            //                 seedGeekArtistId: '$_id.artistId', //
            //                 seedGeekVenueId: '$seedGeekVenueId',
            //                 venueName: '$venueName',
            //                 artistName: '$artistName',
            //                 latitude: '$latitude',
            //                 longitude: '$longitude',
            //                 ticketName: '$ticketName',
            //                 ticketPrice: '$ticketPrice',
            //                 ticketType: '$ticketType',
            //                 quantityLeft: '$quantityLeft',
            //                 eventDate: '$eventDate',
            //                 timings: '$timings'
            //             }
            //         }
            //
            //     ];
            // }
            //
            // else {
            //     var criteria = [
            //         {$match: {seedGeekVenueId: payloadData.venueId, quantityLeft: {$gt: 0}}},
            //         {
            //             $group: {
            //                 originalId: {$first: '$_id'}, // Hold onto original ID.
            //                 _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
            //                 seedGeekVenueId: {$first: '$seedGeekVenueId'},
            //                 venueName: {$first: '$venueName'},
            //                 artistName: {$first: '$artistName'},
            //                 latitude: {$first: '$latitude'},
            //                 longitude: {$first: '$longitude'},
            //                 ticketName: {$first: '$ticketName'},
            //                 ticketPrice: {$first: '$ticketPrice'},
            //                 ticketType: {$first: '$ticketType'},
            //                 quantityLeft: {$first: '$quantityLeft'},
            //                 eventDate: {$first: '$eventDate'},
            //                 timings: {$first: '$timings'}
            //             }
            //
            //         }, {
            //             // this receives the output from the first aggregation.
            //             // So the (originally) non-unique 'id' field is now
            //             // present as the _id field. We want to rename it.
            //             $project: {
            //                 _id: '$originalId', // Restore original ID.
            //                 seedGeekEventId: '$_id.eventId',
            //                 seedGeekArtistId: '$_id.artistId', //
            //                 seedGeekVenueId: '$seedGeekVenueId',
            //                 venueName: '$venueName',
            //                 artistName: '$artistName',
            //                 latitude: '$latitude',
            //                 longitude: '$longitude',
            //                 ticketName: '$ticketName',
            //                 ticketPrice: '$ticketPrice',
            //                 ticketType: '$ticketType',
            //                 quantityLeft: '$quantityLeft',
            //                 eventDate: '$eventDate',
            //                 timings: '$timings'
            //             }
            //         }
            //
            //     ];
            // }


            if (!payloadData.ticketType && !payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }

                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    },
                    {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }


            else if (!payloadData.ticketType && !payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }


                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            eventDate: payloadData.eventDate,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }


                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            eventDate: payloadData.eventDate,
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }

                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && !payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }

                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (payloadData.ticketType && !payloadData.eventDate && !payloadData.searchString) {

                console.log("here aaya");

                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            ticketType: payloadData.ticketType,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }


                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];

                console.log("criteria ", criteria);

            }

            else if (!payloadData.ticketType && payloadData.eventDate && payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            eventDate: payloadData.eventDate,
                            $text: {$search: payloadData.searchString},
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }

                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }

            else if (!payloadData.ticketType && payloadData.eventDate && !payloadData.searchString) {
                var criteria = [
                    {
                        $match: {
                            seedGeekVenueId: payloadData.venueId,
                            quantityLeft: {$gt: 0},
                            eventDate: payloadData.eventDate,
                            isAvailableForPurchase: true,
                            isFaulty: false
                        }


                    },
                    {
                        $group: {
                            originalId: {$first: '$_id'}, // Hold onto original ID.
                            _id: {"artistId": '$seedGeekArtistId', "eventId": "$seedGeekEventId"}, // Set the unique identifier
                            seedGeekVenueId: {$first: '$seedGeekVenueId'},
                            venueName: {$first: '$venueName'},
                            artistName: {$first: '$artistName'},
                            latitude: {$first: '$latitude'},
                            longitude: {$first: '$longitude'},
                            ticketName: {$first: '$ticketName'},
                            ticketPrice: {$first: '$ticketPrice'},
                            ticketType: {$first: '$ticketType'},
                            quantityLeft: {$first: '$quantityLeft'},
                            eventDate: {$first: '$eventDate'},
                            timings: {$first: '$timings'}
                        }

                    }, {
                        // this receives the output from the first aggregation.
                        // So the (originally) non-unique 'id' field is now
                        // present as the _id field. We want to rename it.
                        $project: {
                            _id: '$originalId', // Restore original ID.
                            seedGeekEventId: '$_id.eventId',
                            seedGeekArtistId: '$_id.artistId', //
                            seedGeekVenueId: '$seedGeekVenueId',
                            venueName: '$venueName',
                            artistName: '$artistName',
                            latitude: '$latitude',
                            longitude: '$longitude',
                            ticketName: '$ticketName',
                            ticketPrice: '$ticketPrice',
                            ticketType: '$ticketType',
                            quantityLeft: '$quantityLeft',
                            eventDate: '$eventDate',
                            timings: '$timings'
                        }
                    }

                ];
            }


            Service.TicketService.getTicketAggregate(criteria, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("result all", result);
                    allTickets = result;
                    if (allTickets.length) {
                        for (var i = 0; i < allTickets.length; i++) {
                            (function (i) {
                                allTickets[i].eventDate = dateChangeToFormat(allTickets[i].eventDate);
                                if (i == allTickets.length - 1) {
                                    cb();
                                }

                            }(i))
                        }
                    }
                    else {
                        cb();
                    }
                }
            });

        }],

        getVenueImage: ['checkForAccessToken', function (cb) {
            var criteria = {};

            var projection = {};
            var option = {
                lean: true
            };
            Service.editableFieldService.getEditableFields(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        venueImage = result[0].venueImage;
                    }
                    cb();
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {upcomingTickets: upcomingTickets, allTickets: allTickets, venueImage: venueImage});
        }
    })
};


var getProfile = function (payloadData, callback) {

    var userData;

    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {
                firstName: 1,
                lastName: 1,
                profilePicURL: 1,
                userName: 1,
                email: 1,
                phoneNumber: 1
            };
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        }
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {userData: userData});
        }

    })


};


var updateProfile = function (payloadData, callback) {
    var userData;
    var profilePicURL = {};

    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        console.log(userData)
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {
            if (payloadData.profilePic && payloadData.profilePic.filename) {
                var randomNo = Math.random();
                UploadManager.uploadFile(payloadData.profilePic, randomNo, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL.original = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        profilePicURL.thumbnail = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            } else {
                cb();
            }
        }],
        updateProfile: ['uploadImage', function (cb) {
            var criteria = {
                _id: userData._id
            };
            if (payloadData.profilePic) {
                var setQuery = {
                    firstName: payloadData.firstName,
                    lastName: payloadData.lastName,
                    profilePicURL: profilePicURL
                };
            }
            else {
                var setQuery = {
                    firstName: payloadData.firstName,
                    lastName: payloadData.lastName
                };
            }

            var options = {
                new: true,
                upsert: true
            };

            Service.UserService.updateUser(criteria, setQuery, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userData = result;
                    cb();
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {userData: userData});
        }

    })

};


var changePassword = function (payloadData, callback) {
    var userData;

    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        checkPassword: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: userData._id,
                password: UniversalFunctions.CryptData(payloadData.oldPassword)
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    if (dataAry.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD);
                    }
                }
            });
        }],
        resetPassword: ['checkPassword', function (cb) {
            var criteria = {
                _id: userData._id
            };
            var setQuery = {
                password: UniversalFunctions.CryptData(payloadData.newPassword)
            };
            Service.UserService.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null);
        }

    })

};


var getCards = function (payloadData, callback) {

    var cardData;

    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cardData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        }
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {cardData: cardData});
        }

    })


};


var addCard = function (payloadData, callback) {

    var cardData;
    var userId;
    var userEmail;
    var gatewayId;
    var cardLength = 0;

    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        userEmail = result[0].email;
                        cardLength = result[0].cards.length;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },

        addCard: ['checkForAccessToken', function (cb) {

            stripe.customers.create({
                email: userEmail,
                source: payloadData.stripeToken
            }, function (err, customer) {

                if (err) {
                    cb(err);
                }
                else {
                    gatewayId = customer.id;
                    console.log(gatewayId);
                    var criteria = {
                        _id: userId
                    };

                    var obj = {
                        last4Digit: payloadData.last4Digit,
                        cardType: payloadData.cardType,
                        gatewayId: gatewayId,
                        isDefault: cardLength == 0 ? true : false,
                        isDeleted: false
                    };

                    var setQuery = {
                        $push: {
                            cards: obj
                        }
                    };
                    Service.UserService.updateUser(criteria, setQuery, {
                        new: true,
                        upsert: true
                    }, function (err, result) {
                        if (err) {
                            cb(err)
                        } else {
                            console.log(result);
                            cb();
                        }
                    });
                }

            });


        }],

        getCardData: ['addCard', function (cb) {
            var criteria = {
                _id: userId
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cardData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {cardData: cardData});
        }

    })
};


var deleteCard = function (payloadData, callback) {

    var userId;
    var cardData;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        deleteCard: ['checkForAccessToken', function (cb) {
            var criteria = {
                _id: userId,
                "cards._id": payloadData.cardId
            };

            var setQuery = {
                "$set": {"cards.$.isDeleted": true}
            };

            Service.UserService.updateUser(criteria, setQuery, {new: true, upsert: true}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }],
        getCardData: ['deleteCard', function (cb) {
            var criteria = {
                _id: userId
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cardData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {cardData: cardData});
        }

    })

};


var changeDefaultCard = function (payloadData, callback) {

    var userId;
    var cardData;
    var cardLength;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cardLength = result[0].cards.length;

                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        makeAllDefaultCardFalse: ['checkForAccessToken', function (cb) {

            var criteria = {
                _id: userId
            };

            var x = {};

            for (var i = 0; i < cardLength; i++) {
                var y = "cards." + i + ".isDefault";
                x[y] = false;
            }

            var setQuery = {
                "$set": x
            };

            Service.UserService.updateMultiUser(criteria, setQuery, {
                upsert: false,
                multi: true
            }, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log(result);
                    cb();
                }
            });
        }],
        makeDefaultCard: ['makeAllDefaultCardFalse', function (cb) {
            var criteria = {
                _id: userId,
                "cards._id": payloadData.cardId
            };

            var setQuery = {
                "$set": {"cards.$.isDefault": true}
            };

            Service.UserService.updateUser(criteria, setQuery, {new: true, upsert: true}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cardData = result.cards;
                    cb();
                }
            });
        }],
        getCardData: ['makeDefaultCard', function (cb) {
            var criteria = {
                _id: userId
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cardData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {cardData: cardData});
        }

    })
};


var getTicketDetails = function (payloadData, callback) {
    var userId;
    var tickets;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        getTicketDetails: ['checkForAccessToken', function (cb) {
            var criteria = {
                seedGeekVenueId: payloadData.venueId,
                seedGeekEventId: payloadData.eventId,
                quantityLeft: {$gt: 0}
            };

            var projection = {
                _id: 1,
                venueName: 1,
                artistName: 1,
                eventDate: 1,
                ticketPrice: 1,
                quantityLeft: 1,
                timings: 1
            };
            var option = {
                lean: true
            };
            Service.TicketService.getTicket(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    tickets = result;
                    if (tickets.length) {
                        for (var i = 0; i < tickets.length; i++) {
                            (function (i) {
                                tickets[i].eventDate = dateChangeToFormat(tickets[i].eventDate);
                                tickets[i].eventDate = tickets[i].timings + " " + tickets[i].eventDate;
                                if (i == tickets.length - 1) {
                                    cb();
                                }

                            }(i))
                        }
                    }
                    else {
                        cb();
                    }
                }
            });

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {ticketDetails: tickets});
        }

    })

};


var purchaseTicket = function (payloadData, callback) {

    console.log("purchase ticket ",payloadData);

    var userId;
    var defaultCardId;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {
                _id: 1,
                cards: 1
            };
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        if (result[0].cards.length) {
                            for (var i = 0; i < result[0].cards.length; i++) {

                                if (result[0].cards[i].isDefault == true && result[0].cards[i].isDeleted == false) {
                                    defaultCardId = result[0].cards[i].gatewayId;
                                    break;
                                }

                            }
                            cb();
                        }
                        else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ADD_CARD);
                        }

                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },

        makePayment: ['checkForAccessToken', function (cb) {
            var amountInCents = payloadData.totalAmount * 100;

            stripe.charges.create({
                amount: amountInCents,
                currency: "usd",
                customer: defaultCardId
            }, function (err, charge) {
                if (err) {
                    console.log(err);
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.FAULTY_CARD);
                }
                else {
                    cb();
                }

            });

        }],
        updateTicket: ['makePayment', function (cb) {
            payloadData.ticketData = JSON.parse(payloadData.ticketData);

            for (var i = 0; i < payloadData.ticketData.length; i++) {
                (function (i) {
                    payloadData.ticketData[i].userTicketType = "PURCHASED";
                    payloadData.ticketData[i].userTicketCode = 0;

                }(i))
            }

            var criteria = {
                _id: userId
            };

            var setQuery = {
                $push: {
                    tickets: {
                        $each: payloadData.ticketData
                    }
                }
            };

            Service.UserService.updateUser(criteria, setQuery, {upsert: true}, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }],
        updateTotalTickets: ['updateTicket', function (cb) {
            for (var i = 0; i < payloadData.ticketData.length; i++) {
                (function (i) {
                    console.log(i);
                    var criteria = {
                        _id: payloadData.ticketData[i].ticketId
                    };

                    console.log(parseInt(payloadData.ticketData[i].ticketQuantity));

                    var setQuery = {
                        $inc: {quantityLeft: -parseInt(payloadData.ticketData[i].ticketQuantity)}
                    };

                    Service.TicketService.updateTicket(criteria, setQuery, {new: true}, function (err, result) {

                        console.log(result);
                        if (i == payloadData.ticketData.length - 1) {
                            cb();
                        }

                    });

                }(i))
            }

        }],

        updateUserWallet:['updateTicket',function(cb)
        {
            var tasks = [];

            for(var i = 0 ; i < payloadData.ticketData.length ; i++)
            {
                tasks.push(function(e)
                {
                    return function(cb1){
                        var y = "ADMIN";
                        var uploaderId;
                        async.waterfall([

                            function(cb){

                                var criteria = {
                                    _id: payloadData.ticketData[e].ticketId
                                };

                                var projection = {
                                    uploadedBy: 1,
                                    uploadedById:1
                                };
                                var option = {
                                    lean: true
                                };
                                Service.TicketService.getTicket(criteria, projection, option, function (err, result) {

                                    if(result[0].uploadedBy == "USER")
                                    {
                                        y = "USER";
                                        uploaderId = result[0].uploadedById.userId;
                                        cb(null);
                                    }
                                    else{
                                        cb(null);
                                    }

                                })

                            },

                            function(cb){

                                if(y == "USER")
                                {
                                    var criteria = {
                                        _id: payloadData.userId
                                    };

                                    var dataToSet = {
                                        $inc: {wallet: parseInt(payloadData.ticketData[e].ticketAmount)}
                                    };
                                    var option = {
                                        new: true
                                    };
                                    Service.UserService.updateUser(criteria, dataToSet, option, function (err, result) {
                                        cb(null);

                                    })
                                }
                                else{
                                    console.log("admin ki ticket thi bc");
                                    cb(null);
                                }
                            }
                        ],function(err,result)
                        {
                            cb1(null);
                        })
                    }

                }(i))

            }

            async.series(tasks,function(err,result)
            {
                console.log("err ",err);
                cb(null);
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {});
        }

    })

};


var listUserTickets = function (payloadData, callback) {

    var userId;
    var tickets = [];
    var myTickets = [];
    var sellingTickets = [];
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true,
                tickets: {sort: {userTicketCode: 1}}
            };
            var populate = [
                {
                    path: "tickets.ticketId",
                    match: {},
                    select: '_id venueName seedGeekVenueId seedGeekEventId artistName timings ticketImage eventDate',
                    options: {lean: true}
                }
            ];
            Service.UserService.getUserPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    console.log(err);
                    cb(err)
                } else {
                    if (result.length) {
                        console.log(result[0].tickets);
                        userId = result[0]._id;
                        tickets = result[0].tickets;
                        if (tickets.length) {
                            for (var i = 0; i < tickets.length; i++) {
                                (function (i) {

                                    console.log(tickets[i].ticketId.eventDate);
                                    tickets[i].eventDate = dateChangeToFormat(tickets[i].ticketId.eventDate);
                                    if (i == tickets.length - 1) {
                                        cb();
                                    }

                                }(i))
                            }
                        }
                        else {
                            tickets = [];
                            cb();
                        }
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        getMyTickets: ['checkForAccessToken', function (cb) {

            if (tickets.length) {
                for (var i = 0; i < tickets.length; i++) {
                    (function (i) {
                        if (tickets[i].userTicketType == "PURCHASED" || tickets[i].userTicketType == "UPLOADED" || tickets[i].userTicketType == "GIFTED") {
                            myTickets.push(tickets[i]);
                        }
                        if (i == tickets.length - 1) {
                            cb();
                        }

                    }(i))
                }
            }
            else {
                cb();
            }

        }],
        getSellingTickets: ['checkForAccessToken', function (cb) {

            if (tickets.length) {
                for (var i = 0; i < tickets.length; i++) {
                    (function (i) {
                        if (tickets[i].userTicketType == "TRANSFERRED" || tickets[i].userTicketType == "SOLD") {
                            sellingTickets.push(tickets[i]);
                        }
                        if (i == tickets.length - 1) {
                            cb();
                        }

                    }(i))
                }
            }
            else {
                cb();
            }

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {myTickets: myTickets, sellingTickets: sellingTickets});
        }

    })

};


var userLogout = function (payloadData, callback) {
    var userId;
    var defaultCardId;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();

                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },

        cleartoken: ['checkForAccessToken', function (cb) {
            var criteria = {
                id: userId
            };
            var setQuery = {
                deviceToken: '',
                accessToken: ''
            };
            Service.UserService.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {});
        }

    })

};


var sellYourTicket = function (payloadData, callback) {
    var userId;
    var tickets;
    var insertReplica = false;
    var ticketReplica;
    var newTicketId;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {
                _id: 1,
                tickets: 1
            };
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        tickets = result[0].tickets;
                        cb();

                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },

        updateTickets: ['checkForAccessToken', function (cb) {

            if(payloadData.ticketType == "UPLOADED")
            {
                var criteria = {
                    _id: payloadData.ticketId
                };
                var setQuery = {
                    isAvailableForPurchase: true
                };
                Service.TicketService.updateTicket(criteria, setQuery, {new: true}, function (err, dataAry) {
                    if (err) {
                        cb(err)
                    } else {
                        cb();
                    }
                });
            }
            else{
                insertReplica = true;
                cb();
            }

        }],


        insertNewEntryInTickets:['updateTickets',function(cb)
        {
            if(insertReplica == true)
            {
                var criteria = {
                    _id: payloadData.ticketId
                };
                var projection = {

                };
                var options = {
                    lean: true
                };
                Service.TicketService.getTicket(criteria,projection,options,function(err,result)
                {
                    ticketReplica = result[0];

                    var dataToInsert = {
                        seedGeekVenueId: ticketReplica.seedGeekVenueId,
                        seedGeekEventId: ticketReplica.seedGeekEventId,
                        venueName: ticketReplica.venueName,
                        seedGeekArtistId: ticketReplica.seedGeekArtistId,
                        artistName: ticketReplica.artistName,
                        latitude: ticketReplica.latitude,
                        longitude: ticketReplica.longitude,
                        eventName: ticketReplica.eventName,
                        ticketType: ticketReplica.ticketType,
                        uploadedById: {
                            userId: userId,
                            adminId: null
                        },
                        uploadedBy: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.TICKETUPLOADEDBY.USER,
                        ticketImage: ticketReplica.ticketImage,
                        eventDate: ticketReplica.eventDate,
                        ticketPrice: ticketReplica.ticketPrice,
                        ticketQuantity: 1,
                        expiryDate: ticketReplica.eventDate,
                        quantityLeft: 1,
                        isReadByOCR: ticketReplica.isReadByOCR,
                        currentLocation: ticketReplica.currentLocation,
                        isAvailableForPurchase: true,
                        timings:ticketReplica.timings
                    };

                    Service.TicketService.createTicket(dataToInsert,function(err,result)
                    {
                        if(err){
                            callback(err)
                        }
                        else{
                            newTicketId = result._id;
                            cb();
                        }
                    })


                })
            }
            else{
                newTicketId = payloadData.ticketId;
                cb();
            }


        }],


        updateUserTicket: ['insertNewEntryInTickets', function (cb) {

            var criteria = {
                _id: userId,
                tickets: {$elemMatch: {_id: payloadData.id}}
            };
            var setQuery = {
                'tickets.$.userTicketType': 'TRANSFERRED',
                'tickets.$.userTicketCode': 3,
                'tickets.$.ticketId':newTicketId
            };
            Service.UserService.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]


    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {});
        }

    })

};


var listUsers = function (payloadData, callback) {
    var userId;
    var users;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();

                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        getUsers: ['checkForAccessToken', function (cb) {


            var criteria = {
                _id: {$ne: userId}
            };
            var projection = {
                _id: 1,
                email: 1,
                userName: 1,
                profilePicURL: 1,
                firstName: 1,
                lastName: 1
            };
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    users = result;
                    if (users.length) {
                        users.forEach(function (user) {
                            user.name = user.firstName + " " + user.lastName;
                        });
                        cb();
                    }

                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {users: users});
        }

    })

};


var transferTicket = function (payloadData, callback) {

    var userId;
    var ticket;
    var tickets;
    var myTickets = [];
    var sellingTickets = [];
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken

            };

            var projection = {
                tickets: {$elemMatch: {_id: payloadData.id}}
            };
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        console.log(result);
                        userId = result[0]._id;
                        ticket = result[0].tickets;
                        console.log(ticket);
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        transferTicket: ['checkForAccessToken', function (cb) {

            var criteria = {
                _id: userId,
                tickets: {$elemMatch: {_id: payloadData.id}}
            };
            var setQuery = {
                'tickets.$.userTicketType': 'GIFTED',
                'tickets.$.userTicketCode': 2
            };
            Service.UserService.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }],
        transferTicketToOther: ['transferTicket', function (cb) {


            ticket[0].userTicketType = "PURCHASED";
            ticket[0].userTicketCode = 0;

            var query = {_id: payloadData.userId};

            var options = {lean: true};

            var setData = {
                $push: {
                    tickets: ticket[0]
                }
            };

            Service.UserService.updateUser(query, setData, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {

                    cb(null)
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {});
        }
    });

};


var revertBackTicket = function (payloadData, callback) {
    var userId;
    var tickets;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        tickets = result[0].tickets;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        revertBackTicket: ['checkForAccessToken', function (cb) {

            var criteria = {
                _id: userId,
                tickets: {$elemMatch: {_id: payloadData.id}}
            };
            var setQuery = {
                'tickets.$.userTicketType': 'UPLOADED',
                'tickets.$.userTicketCode': 1
            };
            Service.UserService.updateUser(criteria, setQuery, {upsert: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }],
        updateTicket: ['revertBackTicket', function (cb) {

            var criteria = {
                _id: payloadData.ticketId
            };
            var setQuery = {
               isAvailableForPurchase: false
            };
            Service.TicketService.updateTicket(criteria, setQuery, {upsert: true}, function (err, dataAry) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {});
        }
    });
};


var uploadTicket = function (payloadData, callback) {

    var userId;
    var ticketImageURL;
    var ticketId;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {

            if (payloadData.ticketImage && payloadData.ticketImage.filename) {
                var randomNo = Math.random()*100;
                UploadManager.uploadFile(payloadData.ticketImage, randomNo, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        ticketImageURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            } else {
                cb();
            }
        }],
        addTicket: ['uploadImage', function (cb) {
            var obj = {
                seedGeekVenueId: payloadData.venueId,
                seedGeekEventId: payloadData.eventId,
                venueName: payloadData.venueName,
                seedGeekArtistId: payloadData.artistId ? payloadData.artistId : 0,
                artistName: payloadData.artistName ? payloadData.artistName : "",
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                eventName: payloadData.eventName,
                ticketType: payloadData.ticketType,
                uploadedById: {
                    userId: userId,
                    adminId: null
                },
                uploadedBy: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.TICKETUPLOADEDBY.USER,
                ticketImage: {
                    original: ticketImageURL,
                    thumbnail: ticketImageURL
                },
                eventDate: payloadData.eventDate,
                ticketPrice: payloadData.ticketPrice,
                ticketQuantity: 1,
                expiryDate: payloadData.eventDate,
                quantityLeft: 1,
                isReadByOCR: payloadData.isReadByOCR,
                currentLocation: [payloadData.longitude, payloadData.latitude],
                isAvailableForPurchase: false,
                timings:payloadData.timings
            };
            Service.TicketService.createTicket(obj, function (err, result) {
                if (err) {
                    cb(null)
                }
                else {
                    ticketId = result._id;
                    cb(null);
                }
            })

        }],
        addTicketToUser: ['addTicket', function (cb) {
            var criteria = {
                _id: userId
            };

            var obj = {
                ticketId: ticketId,
                ticketQuantity: 1,
                ticketAmount: payloadData.ticketPrice,
                userTicketType: 'UPLOADED',
                userTicketCode: 1
            };

            var setQuery = {
                $push: {
                    tickets: obj
                }
            };
            Service.UserService.updateUser(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {});
        }
    });

};


var searchForEvents = function (payloadData, callback) {
    var Events = [];
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });

        },
        searchForEvents: ['checkForAccessToken', function (cb) {

            request(Config.seatGeek.baseUrl + "events?client_id=" + Config.seatGeek.clientId + "&client_secret=" + Config.seatGeek.clientSecret + "&q=" + payloadData.eventName, function (err, data, response) {
                if (err) {
                    cb(err);
                }
                else {
                    response = JSON.parse(response);
                    response.events.forEach(function (events) {
                        Events.push({
                            "eventId": events.id,
                            "eventName": events.title,
                            "eventDate": events.datetime_local,
                            "eventDateFormatted": dateChangeToFormatEvent(events.datetime_local)
                        })
                    });
                    cb();
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {events: Events})
        }
    })

};


var searchForArtistEventWise = function (payloadData, callback) {
    console.log(payloadData);
    var Venues = [];
    var Artists = [];
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });

        },
        searchForVenues: ['checkForAccessToken', function (cb) {

            request(Config.seatGeek.baseUrl + "events/" + payloadData.eventId + "?client_id=" + Config.seatGeek.clientId + "&client_secret=" + Config.seatGeek.clientSecret, function (err, data, response) {
                if (err) {
                    cb(err);
                }
                else {
                    console.log(err);
                    console.log(response);
                    response = JSON.parse(response);
                    Venues.push({
                        "venueId": response.venue.id,
                        "venueName": response.venue.name,
                        "location": response.venue.location
                    });
                    cb();
                }
            })
        }],
        searchForArtists: ['checkForAccessToken', function (cb) {

            request(Config.seatGeek.baseUrl + "events/" + payloadData.eventId + "?client_id=" + Config.seatGeek.clientId + "&client_secret=" + Config.seatGeek.clientSecret, function (err, data, response) {
                if (err) {
                    cb(err);
                }
                else {
                    response = JSON.parse(response);
                    response.performers.forEach(function (artists) {
                        Artists.push({
                            "artistId": artists.id,
                            "artistName": artists.name
                        })
                    });
                    cb();
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {artists: Artists, venues: Venues})
        }
    })

};


var sellTicketOnSellingScreen = function (payloadData, callback) {
    var userId;
    var ticketImageURL;
    var ticketId;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {

            if (payloadData.ticketImage && payloadData.ticketImage.filename) {
                var randomNo = Math.random();
                UploadManager.uploadFile(payloadData.ticketImage, randomNo, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        ticketImageURL = UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original;
                        cb();
                    }
                })
            } else {
                cb();
            }
        }],
        addTicket: ['uploadImage', function (cb) {
            var obj = {
                seedGeekVenueId: payloadData.venueId,
                seedGeekEventId: payloadData.eventId,
                venueName: payloadData.venueName,
                seedGeekArtistId: payloadData.artistId ? payloadData.artistId : 0,
                artistName: payloadData.artistName ? payloadData.artistName : "",
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                eventName: payloadData.eventName,
                ticketType: payloadData.ticketType,
                uploadedById: {
                    userId: userId,
                    adminId: null
                },
                uploadedBy: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.TICKETUPLOADEDBY.USER,
                ticketImage: {
                    original: ticketImageURL,
                    thumbnail: ticketImageURL
                },
                eventDate: payloadData.eventDate,
                ticketPrice: payloadData.ticketPrice,
                ticketQuantity: 1,
                expiryDate: payloadData.eventDate,
                quantityLeft: 1,
                isReadByOCR: payloadData.isReadByOCR,
                currentLocation: [payloadData.longitude, payloadData.latitude],
                isAvailableForPurchase: true
            };
            Service.TicketService.createTicket(obj, function (err, result) {
                if (err) {
                    cb(null)
                }
                else {
                    ticketId = result._id;
                    cb(null);
                }
            })

        }],
        addTicketToUser: ['addTicket', function (cb) {
            var criteria = {
                _id: userId
            };

            var obj = {
                ticketId: ticketId,
                ticketQuantity: 1,
                ticketAmount: payloadData.ticketPrice,
                userTicketType: 'TRANSFERRED',
                userTicketCode: 3
            };

            var setQuery = {
                $push: {
                    tickets: obj
                }
            };
            Service.UserService.updateUser(criteria, setQuery, {
                new: true,
                upsert: true
            }, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb();
                }
            });

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {});
        }
    });

};


var getSuggestedPrice = function (payloadData, callback) {

    var lowestPrice = 0;
    var sellingFormula;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();

                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        getLowestPrice: ['checkForAccessToken', function (cb) {


            var criteria = {
                seedGeekEventId: payloadData.eventId,
                quantityLeft: {$gt: 0},
                isAvailableForPurchase: true,
                isFaulty: false
            };
            var projection = {
                ticketPrice: 1
            };
            var option = {
                lean: true
            };
            Service.TicketService.getTicket(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {

                    if (result.length) {
                        lowestPrice = Math.min.apply(Math, result.map(function (o) {
                            return o.ticketPrice;
                        }))
                    }
                    cb();

                }
            });
        }],
        getSuggestedPrice: ['getLowestPrice', function (cb) {

            var criteria = {};
            var projection = {
                sellingFormula: 1
            };
            var option = {
                lean: true
            };
            Service.editableFieldService.getEditableFields(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {

                    console.log(result);
                    if (result.length) {
                        sellingFormula = result[0].sellingFormula;
                        sellingFormula = parseInt(sellingFormula) + parseInt(lowestPrice);
                        cb();
                    }
                    else {
                        cb();
                    }

                }
            });

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {suggestedPrice: sellingFormula});
        }

    })

};


function dateChangeToFormat(getdate) {
    var dateObj = new Date(getdate);
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var year = dateObj.getUTCFullYear();
    var days = ['SUN', 'MON', 'TUES', 'WED', 'THUR', 'FRI', 'SAT'];
    var weekDay = days[dateObj.getDay()];
    year = year.toString();
    console.log(year);

    var x = year.split("");
    return (weekDay + " " + month + "/" + x[2]+x[3]);
}


function dateChangeToFormatEvent(getdate) {
    var dateObj = new Date(getdate);
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var year = dateObj.getUTCFullYear();
    var date1 = dateObj.getDate();
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var hours = dateObj.getHours();
    var minutes = dateObj.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + '' + ampm;

    var weekDay = days[dateObj.getDay()];

    return (strTime + " " + weekDay + "," + date1 + "/" + month + "/" + year);
}


module.exports = {

    userSignUp: userSignUp,
    fbLogin: fbLogin,
    emailLogin: emailLogin,
    forgotPassword: forgotPassword,
    checkForEmailAndPhone: checkForEmailAndPhone,
    searchSuggestions: searchSuggestions,
    accessTokenLogin: accessTokenLogin,
    loadTicketsOfArtists: loadTicketsOfArtists,
    loadTicketsOfVenues: loadTicketsOfVenues,
    updateProfile: updateProfile,
    changePassword: changePassword,
    getCards: getCards,
    addCard: addCard,
    deleteCard: deleteCard,
    changeDefaultCard: changeDefaultCard,
    getProfile: getProfile,
    getTicketDetails: getTicketDetails,
    purchaseTicket: purchaseTicket,
    listUserTickets: listUserTickets,
    userLogout: userLogout,
    sellYourTicket: sellYourTicket,
    listUsers: listUsers,
    transferTicket: transferTicket,
    revertBackTicket: revertBackTicket,
    uploadTicket: uploadTicket,
    searchForEvents: searchForEvents,
    searchForArtistEventWise: searchForArtistEventWise,
    sellTicketOnSellingScreen: sellTicketOnSellingScreen,
    getSuggestedPrice: getSuggestedPrice


};