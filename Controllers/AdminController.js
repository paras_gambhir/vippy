'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var Config = require('../Config');
var SeedGeekConfig = require('../Config/seatGeekConfig');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');

var request = require('request');


var adminLogin = function (payloadData, callback) {
    var adminData;
    var accessToken;
    async.auto({
        checkForEmail: function (cb) {

            var criteria = {
                Email: payloadData.email,
                Password: UniversalFunctions.CryptData(payloadData.password)
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminService.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        adminData = result[0];
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS);
                    }
                }
            });
        },
        setToken: ['checkForEmail', function (cb) {
            var tokenData = {
                id: adminData._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        adminData.accessToken = accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {adminData: adminData});
        }

    })

};


var getUsers = function (payloadData, callback) {
    callback(null, {userData: []})
};


var getAnalyticsData = function (payloadData, callback) {
    callback(null, {analyticsData: []})
};


var refundMoney = function (payloadData, callback) {
    callback(null)
};


var addTicket = function (payloadData, callback) {
    var ticket = {};
    var userData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminService.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {

            if (payloadData.images) {
                var id = userData._id + new Date().getTime();
                console.log("============================", id, userData._id);
                UploadManager.uploadFileToS3WithThumbnail(payloadData.images, id, function (err, result) {
                    if (err) {
                        console.log("here err", err);
                        cb();
                    }
                    else {
                        ticket.original = Config.awsS3Config.s3BucketCredentials.s3URL + result.original;
                        ticket.thumbnail = Config.awsS3Config.s3BucketCredentials.s3URL + result.thumbnail;
                        console.log("============================", ticket);

                        cb(null);
                    }
                })
            }
            else {
                ticket.original = 'https://s3-us-west-2.amazonaws.com/vippy/default-person.jpg';
                ticket.thumbnail = 'https://s3-us-west-2.amazonaws.com/vippy/default-person.jpg';
                cb(null)
            }

        }],
        addTickets: ['uploadImage', function (cb) {

            var obj = {
                seedGeekVenueId: payloadData.seedGeekVenueId,
                seedGeekEventId: payloadData.seedGeekEventId,
                venueName: payloadData.venueName,
                seedGeekArtistId: payloadData.seedGeekArtistId ? payloadData.seedGeekArtistId : 0,
                artistName: payloadData.artistName ? payloadData.artistName : "",
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                eventName: payloadData.eventName,
                ticketType: payloadData.ticketType,
                uploadedById: {
                    userId: null,
                    adminId: userData._id
                },
                uploadedBy: Config.APP_CONSTANTS.DATABASE.TICKETUPLOADEDBY.ADMIN,
                ticketImage: ticket,
                eventDate: payloadData.eventDate,
                ticketPrice: payloadData.ticketPrice,
                ticketQuantity: payloadData.ticketQuantity,
                expiryDate: payloadData.expiryDate,
                quantityLeft: payloadData.ticketQuantity,
                isReadByOCR: false,
                currentLocation: [payloadData.longitude, payloadData.latitude],
                isAvailableForPurchase: true
            };
            Service.TicketService.createTicket(obj, function (err, result) {
                if (err) {
                    cb(null)
                }
                else {
                    cb(null);
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {})
        }
    });
};


var imageUploadArtistVenue = function (payloadData, callback) {
    var images;
    var userData;
    async.auto({
        checkForAccessToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminService.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userData = result[0];
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });
        },
        uploadImage: ['checkForAccessToken', function (cb) {

            if (payloadData.images) {
                var id = userData._id + payloadData.type;
                UploadManager.uploadFileToS3WithThumbnail(payloadData.images, id, function (err, result) {
                    if (err) {
                        console.log("here err", err);
                        cb();
                    }
                    else {
                        images = Config.awsS3Config.s3BucketCredentials.s3URL + result.original;
                        // images = Config.awsS3Config.s3BucketCredentials.s3URL+result.thumbnail;
                        cb(null);
                    }
                })
            }
            else {
                images = 'https://s3-us-west-2.amazonaws.com/vippy/default-person.jpg';
                //   images.thumbnail =  'https://s3-us-west-2.amazonaws.com/vippy/default-person.jpg';
                cb(null)
            }

        }],
        images: ['uploadImage', function (cb) {
            var dataToSet = {};
            var obj = {};
            if (payloadData.type == 1) {

                dataToSet = {
                    venueImage: images
                };
            }
            else {
                dataToSet = {
                    artistImage: images
                };
            }
            Service.editableFieldService.updateEditableFields(obj, dataToSet, {}, function (err, result) {
                if (err) {
                    cb(null)
                }
                else {
                    cb(null);
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err)
        }
        else {
            callback(null, {})
        }
    });
};


var searchForEvents = function (payloadData, callback) {
    var Events = [];
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminService.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });

        },
        searchForEvents: ['checkForAccessToken', function (cb) {

            request(SeedGeekConfig.seatGeek.baseUrl + "events?client_id=" + SeedGeekConfig.seatGeek.clientId + "&client_secret=" + SeedGeekConfig.seatGeek.clientSecret + "&q=" + payloadData.searchString, function (err, data, response) {
                if (err) {
                    cb(err);
                }
                else {
                    //  console.log(response)
                    response = JSON.parse(response);
                    response.events.forEach(function (events) {
                        Events.push({
                            "eventId": events.id,
                            "eventName": events.title,
                            "eventDate": events.datetime_local,
                            "eventDateFormatted": dateChangeToFormatEvent(events.datetime_local)
                        })
                    });
                    cb();
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {events: Events})
        }
    })

};


var searchForArtistEventWise = function (payloadData, callback) {
    //console.log(payloadData);
    var Venues = [];
    var Artists = [];
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminService.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_EXPIRED);
                    }
                }
            });

        },
        searchForVenues: ['checkForAccessToken', function (cb) {

            request(SeedGeekConfig.seatGeek.baseUrl + "events/" + payloadData.eventId + "?client_id=" + SeedGeekConfig.seatGeek.clientId + "&client_secret=" + SeedGeekConfig.seatGeek.clientSecret, function (err, data, response) {
                if (err) {
                    cb(err);
                }
                else {
                    console.log(err);
                    console.log(response);
                    response = JSON.parse(response);
                    Venues.push({
                        "venueId": response.venue.id,
                        "venueName": response.venue.name,
                        "location": response.venue.location
                    });
                    cb();
                }
            })
        }],
        searchForArtists: ['checkForAccessToken', function (cb) {

            request(SeedGeekConfig.seatGeek.baseUrl + "events/" + payloadData.eventId + "?client_id=" + SeedGeekConfig.seatGeek.clientId + "&client_secret=" + SeedGeekConfig.seatGeek.clientSecret, function (err, data, response) {
                if (err) {
                    cb(err);
                }
                else {
                    response = JSON.parse(response);
                    response.performers.forEach(function (artists) {
                        Artists.push({
                            "artistId": artists.id,
                            "artistName": artists.name
                        })
                    });
                    cb();
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {artists: Artists, venues: Venues})
        }
    })

};


function dateChangeToFormatEvent(getdate) {
    var dateObj = new Date(getdate);
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var year = dateObj.getUTCFullYear();
    var date1 = dateObj.getDate();
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var hours = dateObj.getHours();
    var minutes = dateObj.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + '' + ampm;

    var weekDay = days[dateObj.getDay()];

    return (strTime + " " + weekDay + "," + date1 + "/" + month + "/" + year);
}


var listTickets = function (payloadData, callback) {

    var ticketData = [];

    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.AdminService.getAdmin(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("result  ",result)
                    if (result.length) {
                        console.log("hello")
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS);
                    }
                }
            });
        },
        getTickets: ['checkForAccessToken', function (cb) {
            var criteria = {
                uploadedBy: payloadData.uploadedBy,
                isDeleted: false
            };

            var projection = {
                _id: 1,
                eventName: 1,
                venueName: 1,
                artistName: 1,
                ticketPrice: 1,
                quantityLeft: 1,
                ticketQuantity: 1,
                ticketImage: 1,
                eventDate: 1,
                expiryDate: 1
            };

            var option = {
                sort: {_id: -1}
            };

            if (payloadData.uploadedBy == "ADMIN") {
                var populate = [
                    {
                        path: 'uploadedById.adminId',
                        match: {},
                        select: "Email",
                        options: {}
                    }
                ]
            }
            else {

                var populate = [
                    {
                        path: 'uploadedById.userId',
                        match: {},
                        select: "email",
                        options: {}
                    }
                ]
            }

            Service.TicketService.getTicketPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                   ticketData = result;
                    cb();
                }
            });

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, {ticketData: ticketData});
        }

    })

};





module.exports = {
    adminLogin: adminLogin,
    getUsers: getUsers,
    getAnalyticsData: getAnalyticsData,
    refundMoney: refundMoney,
    addTicket: addTicket,
    imageUploadArtistVenue: imageUploadArtistVenue,
    searchForEvents: searchForEvents,
    searchForArtistEventWise: searchForArtistEventWise,
    listTickets: listTickets
};