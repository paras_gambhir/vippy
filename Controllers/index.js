/**
 * Created by paras on 3/10/16.
 */
module.exports  = {
    AdminController : require('./AdminController'),
    AppVersionController : require('./AppVersionController'),
    UserController : require('./UserController')

};