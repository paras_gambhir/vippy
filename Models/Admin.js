/**
 * Created by parasgambhir on 03/10/16.
 */




var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');




var Admin = new Schema({
    Email:{type: String, index:true, unique:true,trim:true, required: true},
    Password:{type: String, trim: true, required: true},
    accessToken:{type: String, trim: true, default:""},
    insertedAt:{type: Date, default: Date.now},
    lastLogin:{type: Date, default:Date.now,required: true}
});



module.exports = mongoose.model('Admin', Admin);