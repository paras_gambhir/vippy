var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * Created by parasgambhir on 03/10/16.
 */



var Config = require('../Config');


var userCards = new Schema({
    last4Digit :{type : String,required: true},
    cardType:{type:String,trim:true,required:true},
    gatewayId:{type:String, trim:true,required:true},
    isDefault:{type:Boolean,default:false},
    isDeleted:{type:Boolean,default:false}

});



var userBankAccount = new Schema({
    gatewayId:{type:String,trim:true, default: null, required: true},
    last4Digit:{type: Number, default:0, required: true},
    isDeleted:{type:Boolean,default:false}
});



var userTicketUploadRequests = new Schema({
    //ticketName:{type: String, default: null, required: true},
    //eventName :{type: String, default : null, required : true},
    //artistName: {type : String , default : null, required : true},
    //ticketPrice:{type: Number, default: 0 , required: true},
    //ticketImage:{type: String, default: null, required: false},
    //expiryDate:{type: Date,required: true},
    //isUploadedByAdmin:{type: Boolean, default: false}
    tickedId:{type: Schema.ObjectId,ref:'Tickets',required:true}
});

var userTickets = new Schema({
    ticketId:{type: Schema.ObjectId,ref:'Tickets',default:null,required:true},
    ticketQuantity:{type:Number,default:0, required: true},
    ticketAmount:{type:Number,required: true},
    userTicketType:{
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.USERTICKETTYPE.PURCHASED,
            Config.APP_CONSTANTS.DATABASE.USERTICKETTYPE.SOLD,
            Config.APP_CONSTANTS.DATABASE.USERTICKETTYPE.UPLOADED,
            Config.APP_CONSTANTS.DATABASE.USERTICKETTYPE.TRANSFERRED,
            Config.APP_CONSTANTS.DATABASE.USERTICKETTYPE.GIFTED
        ]
    },
    userTicketCode:{type: Number, required: true}   // 0 - purchased, 1 - uploaded, 2 - gifted , 3 - transferred , 4 - sold
});


var transferredTickets = new Schema({
    transferredTo:{type:Schema.ObjectId, ref:'User',required:true},
    tickedId:{type: Schema.ObjectId,ref:'Tickets',required:true},
    ticketPrice:{type:Number,default:0, required: true},
    transferredDate:{type:Date, default: Date.now, required:true}
});



var giftedTickets = new Schema({
    giftedBy:{type:Schema.ObjectId, ref:'User',required:true},
    tickedId:{type: Schema.ObjectId,ref:'Tickets',required:true},
    ticketPrice:{type:Number,default:0, required: true},
    giftedDate:{type:Date, default: Date.now, required:true}
});



var User = new Schema({
    firstName: {type: String, trim: true, default: null, sparse: true},
    lastName: {type: String, trim: true, default: null, sparse: true},
    userName: {type: String, trim: true, default: null, sparse: true, unique: true},
    phoneNumber:{type: String,unique: true},
    facebookId: {type: String, default: null, trim: true},
    email: {type: String, trim: true, unique: true, index: true, required: true},
    password: {type: String, trim: true,required: true},
    codeUpdatedAt: {type: Date, default: Date.now, required: true},
    registrationDate : {type: Date, default: Date.now, required: true},
    appVersion : {type: String},
    accessToken : {type: String, trim: true, index: true, unique: true, sparse: true},
    deviceToken : {type: String, trim: true},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ]
    },
    isBlocked: {type: Boolean, default: false},
    isFreezed: {type: Boolean, default: false},
    isDeleted:{type:Boolean,default:0},
    latitude: {type: Number,default:0},
    longitude:{type:Number,default:0},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    wallet:{type:Number,default:0},
    cards:[userCards],
    bankAccount:[userBankAccount],
    tickets:[userTickets],
    uploadRequests:[userTicketUploadRequests],
    transferredTickets:[transferredTickets],
    giftedTickets:[giftedTickets],
    pushNotifications:{type: Boolean, default: false, required: false},
    noOfTicketsUploaded:{type: Number, default: 0},
    noOfTicketsPurchased:{type:Number,default:0},
    faultyTicketsUploaded:{type:Number,default:0}
});


User.index({'currentLocation.coordinates': "2d"});


module.exports = mongoose.model('User', User);