/**
 * Created by parasgambhir on 03/10/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');


var orderDetails = new Schema({

    ticketId:{type:Schema.ObjectId, ref:'Tickets',required:true},
    ticketPrice:{type: Number, default:0 , required: true}

});



var Orders = new Schema({

    userId:{type: Schema.ObjectId, ref:'User',required:true},
    purchaseDate:{type: Date, default: Date.now , required : true},
    ticketQuantity:{type:Number, default:0, required:true},
    ticketsInOrder: [orderDetails],
    totalPrice:{type: Number, default:0 , required: true}

});



module.exports = mongoose.model('Orders', Orders);