/**
 * Created by parasgambhir on 03/10/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Tickets = new Schema({
    seedGeekEventId:{type: Number, default:0,required:true},
    eventName:{type:String,trim:true,default:null, required: true},
    seedGeekVenueId:{type: Number, default:0,required:false},
    venueName:{type:String,trim:true,default:null, required: false},
    seedGeekArtistId:{type: Number, default:0,required:false},
    artistName:{type:String,trim:true,default:null, required: false},
    latitude:{type:Number,default:0,required:true},
    longitude:{type:Number,default:0,required:true},
    ticketType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.TICKETTYPE.GA,
            Config.APP_CONSTANTS.DATABASE.TICKETTYPE.VIP
        ]
    },
    uploadedBy: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.TICKETUPLOADEDBY.ADMIN,
            Config.APP_CONSTANTS.DATABASE.TICKETUPLOADEDBY.USER
        ]
    },
    uploadedById: {
        userId:{type:Schema.ObjectId, ref:'User',default: null},
        adminId:{type: Schema.ObjectId, ref :'Admin',default: null}
    },
    currentLocation:{type:[Number], index:'2dsphere'},
    ticketImage:{
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}},
    timings:{type: String, default: null, required: false},
    eventDate:{type:Date,default:Date.now,required:true},
    ticketPrice: {type: Number,default: 0, required : true},
    ticketQuantity:{type:Number, default:0, required: true},
    quantityLeft:{type:Number, default: 0, required: true},
    insertedAt: {type: Date, default: Date.now, required: true},
    expiryDate:{type:Date,default:Date.now,required:true},
    isFaulty:{type:Boolean,default:false},
    isAvailableForPurchase:{type:Boolean, default: false, required: true},
    isDeleted:{type:Boolean,default:false},
    isReadByOCR:{type:Boolean,default:true}
});

Tickets.index({'currentLocation.coordinates': "2dsphere"});


module.exports = mongoose.model('Tickets', Tickets);