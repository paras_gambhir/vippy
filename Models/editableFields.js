/**
 * Created by parasgambhir on 03/10/16.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');


var backgroundImage = new Schema({
    "size":{type:String,required:true},
    "url":{type:String,required:true}
});


var editableFields = new Schema({
    creditCardFees:{type: Number, default: 3, required: true},
    adminSupportEmail:{type: String,trim: true,required: true},
    adminCountryCode:{type:String,trim: true,required: true},
    adminSupportNumber:{type: String, required: true},
    sellingFormula:{type:Number,required:true,default:5},
    artistImage:{type:String,default:"http://defaultrecordings.co.uk/wp-content/uploads/2016/07/DJRansome.jpg"},
    backgroundImage: [backgroundImage],
    venueImage:{type:String,default:"https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSf_1idrgWJoWNUO3momHl-TZyRs1el1sdEGIDcGw7IiLyRUQlr"}
});



module.exports = mongoose.model('editableFields', editableFields);