/**
 * Created by parasgambhir on 08/10/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');


var backgroundImage = new Schema({
    "size":{type:String,required:true},
    "url":{type:String,required:true}
});


var Regions = new Schema({
      regionName:{type:String,default:null,required:true},
      latitude:{type:String,default:null,required:true},
      longitude:{type:String,default:null,required:true},
      radius:{type:Number,default: 30, required:true}
});



module.exports = mongoose.model('Regions', Regions);