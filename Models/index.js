
module.exports = {
    User : require('./User'),
    AppVersions : require('./AppVersions'),
    Tickets: require('./Tickets'),
    Orders: require('./Orders'),
    Admin: require('./Admin'),
    editableFields: require('./editableFields'),
    Regions: require('./Regions')
};