'use strict';
/**
 * Created by prince on 12/7/15.
 */
var mongoose = require('mongoose');
var Config = require('../Config');
var SocketManager = require('../Lib/SocketManager');
var Service = require('../Services');
var async = require('async');

//Connect to MongoDB
mongoose.connect(Config.dbConfig.mongo.URI, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});
exports.bootstrapAdmin = function (callback) {
    var adminData1 = {
        Email: 'paras5217@gmail.com',
        Password: 'e10adc3949ba59abbe56e057f20f883e'
    };
    var adminData2 = {
        Email: 'ppgparas5@gmail.com',
        Password: 'e10adc3949ba59abbe56e057f20f883e'
    };
    async.parallel([
        function (cb) {
            insertData(adminData1.Email, adminData1, cb)
        },
        function (cb) {
            insertData(adminData2.Email, adminData2, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })


};

exports.bootstrapAppVersion = function (callback) {
    var appVersion1 = {
        latestIOSVersion: '100',
        latestAndroidVersion: '100',
        criticalAndroidVersion: '100',
        criticalIOSVersion: '100',
        appType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
    };
    var appVersion2 = {
        latestIOSVersion: '100',
        latestAndroidVersion: '100',
        criticalAndroidVersion: '100',
        criticalIOSVersion: '100',
        appType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER
    };


    async.parallel([
        function (cb) {
            insertVersionData(appVersion1.appType, appVersion1, cb)
        },
        function (cb) {
            insertVersionData(appVersion2.appType, appVersion2, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished For App Version');
    })


};


exports.bootstrapEditableFields = function(callback)
{
    var editableFields = {
        creditCardFees: 3,
        adminSupportEmail: "admin@vippy.com",
        adminCountryCode:"+91",
        adminSupportNumber:9467813457,
        sellingFormula: 5,
        artistImage:"http://defaultrecordings.co.uk/wp-content/uploads/2016/07/DJRansome.jpg",
        venueImage:"https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSf_1idrgWJoWNUO3momHl-TZyRs1el1sdEGIDcGw7IiLyRUQlr"

    };
    async.parallel([
        function (cb) {
            insertEditableFieldsData(editableFields, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })
};



function insertEditableFieldsData(editableFields, callback)
{
    var needToCreate = true;
    async.series([
        function (cb) {
            var criteria = {
            };
            Service.editableFieldService.getEditableFields(criteria, {}, {}, function (err, data) {
                if (data && data.length > 0) {
                    needToCreate = false;
                }
                cb()
            })
        }, function (cb) {
            if (needToCreate) {
                Service.editableFieldService.createEditableFields(editableFields, function (err, data) {
                    cb(err, data)
                })
            } else {
                cb();
            }
        }], function (err, data) {
        console.log('Bootstrapping finished for editable fields');
        callback(err, 'Bootstrapping finished For editable fields Data')
    })

}



function insertVersionData(appType, versionData, callback) {
    var needToCreate = true;
    async.series([
        function (cb) {
        var criteria = {
            appType: appType
        };
        Service.AppVersionService.getAppVersion(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.AppVersionService.createAppVersion(versionData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
        console.log('Bootstrapping finished for ' + appType);
        callback(err, 'Bootstrapping finished For Admin Data')
    })
}

function insertData(email, adminData, callback) {
    var needToCreate = true;
    async.series([function (cb) {
        var criteria = {
            Email: email
        };
        Service.AdminService.getAdmin(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.AdminService.createAdmin(adminData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
        console.log('Bootstrapping finished for ' + email);
        callback(err, 'Bootstrapping finished')
    })
}

//Start Socket Server
exports.connectSocket = SocketManager.connectSocket;


