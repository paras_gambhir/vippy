
module.exports = {
    UserService: require('./UserService'),
    AdminService: require('./AdminService'),
    AppVersionService: require('./AppVersionService'),
    OrderService: require('./OrderService'),
    TicketService: require('./TicketService'),
    editableFieldService: require('./editableFieldService')

};