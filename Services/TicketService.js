/**
 * Created by parasgambhir on 03/10/16.
 */


'use strict';

var Models = require('../Models');

var getTicket= function(criteria,projection,option,callback){
    Models.Tickets.find(criteria,projection,option,callback);
};

var createTicket= function (data,callback) {
    new Models.Tickets(data).save(callback);
};

var deleteTicket= function (criteria,callback) {
    Models.Tickets.remove(criteria,callback)
};
var updateTicket= function (criteria, dataToSet, options, callback) {
    Models.Tickets.update(criteria, dataToSet, options, callback);
};

var getTicketPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Tickets.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var getTicketAggregate = function (criteria,callback) {

    Models.Tickets.aggregate(criteria).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};


module.exports = {
    getTicket: getTicket,
    createTicket: createTicket,
    deleteTicket:deleteTicket,
    updateTicket:updateTicket,
    getTicketPopulate: getTicketPopulate,
    getTicketAggregate:getTicketAggregate
};

