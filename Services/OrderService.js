/**
 * Created by parasgambhir on 03/10/16.
 */

'use strict';

var Models = require('../Models');

var getOrder= function(criteria,projection,option,callback){
    Models.Orders.find(criteria,projection,option,callback);
};

var createOrder= function (data,callback) {
    new Models.Orders(data).save(callback);
};

var deleteOrder= function (criteria,callback) {
    Models.Orders.remove(criteria,callback)
};
var updateOrder= function (criteria, dataToSet, options, callback) {
    Models.Orders.update(criteria, dataToSet, options, callback);
};

var getOrderPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Orders.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

module.exports = {
    getOrder: getOrder,
    createOrder: createOrder,
    deleteOrder:deleteOrder,
    updateOrder:updateOrder,
    getOrderPopulate: getOrderPopulate
};

