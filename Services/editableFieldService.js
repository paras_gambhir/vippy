/**
 * Created by parasgambhir on 03/10/16.
 */

'use strict';

var Models = require('../Models');

var getEditableFields= function(criteria,projection,option,callback){
    Models.editableFields.find(criteria,projection,option,callback);
};

var createEditableFields= function (data,callback) {
    new Models.editableFields(data).save(callback);
};

var deleteEditableFields= function (criteria,callback) {
    Models.editableFields.remove(criteria,callback)
};
var updateEditableFields= function (criteria, dataToSet, options, callback) {
    Models.editableFields.update(criteria, dataToSet, options, callback);
};

var getEditableFieldsPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.editableFields.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

module.exports = {
    getEditableFields: getEditableFields,
    createEditableFields: createEditableFields,
    deleteEditableFields:deleteEditableFields,
    updateEditableFields:updateEditableFields,
    getEditableFieldsPopulate: getEditableFieldsPopulate
};

